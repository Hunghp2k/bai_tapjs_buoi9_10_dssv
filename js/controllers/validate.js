// hợp lệ => return true

function kiemTraTrung(idSV, svArr) {
    // findIndex return vị trí của item nếu điều kiện true, nếu ko tìm thấy sẽ return -1
    var viTri = svArr.findIndex(function (item) {
        return item.maSV == idSV;
    });
    if (viTri != -1) {
        //
        document.getElementById("spanMaSV").innerText = "Mã sinh viên đã tồn tại";
        return false;
    } else {
        document.getElementById("spanMaSV").innerText = "";
        return true;
    }
    console.log(`  🚀: kiemTraTrung -> viTri`, viTri);
}
